

var WifiIcon = L.Icon.extend({
    options: {
        iconSize: [25, 25]
    }
});

var wifi_icon_black = new WifiIcon({iconUrl: "icons/wifi.png"});
var wifi_icon_blue = new WifiIcon({iconUrl: "icons/wifi-blue.png"});
var wifi_icon_green = new WifiIcon({iconUrl: "icons/wifi-green.png"});
var wifi_icon_pink = new WifiIcon({iconUrl: "icons/wifi-pink.png"});
var wifi_icon_purple = new WifiIcon({iconUrl: "icons/wifi-purple.png"});
var wifi_icon_red = new WifiIcon({iconUrl: "icons/wifi-red.png"});


var map = undefined;
var layers_control = L.control.layers(null, null);
var wifi_layers = {};

function onEachFeature(feature, layer) {
    if (!feature.properties || !feature.properties.description) {
        return;
    }
    let description = feature.properties.description;

    let desc_lower = description.toLowerCase();
    if(desc_lower.includes("crypt:wpa2")) {
        layer.setIcon(wifi_icon_blue);
    } else if(desc_lower.includes("crypt:wpa3")) {
        layer.setIcon(wifi_icon_green);
    }  else if(desc_lower.includes("crypt:wpa")) {
        layer.setIcon(wifi_icon_purple);
    } else if(desc_lower.includes("crypt:open")) {
        layer.setIcon(wifi_icon_red);
    } else if(desc_lower.includes("crypt:wep")) {
        layer.setIcon(wifi_icon_pink);
    } else {
        layer.setIcon(wifi_icon_black);
    }

    layer.bindPopup(
        description.replace(/(?:\r\n|\r|\n)/g, '<br>')
    );
}

function main() {
    map = prepare_map();

    const upload_form = document.getElementById("upload-form");
    upload_form.addEventListener("submit", function(e) {
        e.preventDefault();
        const layer_name = this.name.value;
        const data = new FormData(this);
        fetch(`/layer/${layer_name}`, {
            method: "POST",
            body: data,
        })
            .then(res => res.json())
            .then(j => {
                console.log(j);
                if (j["status"] != "OK"){
                    throw j["status"];
                }
                clear_form(this);
                load_layers([layer_name]);
            })
            .catch(error =>
                create_alert(`Error uploading layer: ${error}`)
            );

    });

    document.getElementById("upload-form-input-file")
        .addEventListener("change", function(e) {
            const name_input = document.getElementById("upload-form-input-name");
            if (!name_input.value) {
                const filename = e.target.files[0].name;
                const layer_name = filename.split(".").slice(0, -1).join(".");
                name_input.value = layer_name;
            }
        });

    // lazy loading for layers data
    map.on('overlayadd', overlayadd_hook);
    load_layers();
}

function clear_form(form) {
    var inputs = form.getElementsByTagName('input');
    for (var i = 0; i<inputs.length; i++) {
        switch (inputs[i].type) {
        case "file":
        case 'text':
            inputs[i].value = '';
            break;
        case 'radio':
        case 'checkbox':
            inputs[i].checked = false;
            break;
        }
    }
}

function overlayadd_hook(e) {
    const layer_name = e.name.split("</span")[0].split('">')[1];
    console.log(`Enabling layer ${layer_name}`);

    if (wifi_layers[layer_name].getLayers().length) {
        return;
    }
    fetch(`/layer/${layer_name}`)
        .then(res => res.text())
        .then(kmltext => {
            let geo_data = toGeoJSON.kml(
                new DOMParser().parseFromString(kmltext, 'text/xml')
            );
            wifi_layers[layer_name].addData(geo_data);
        })
        .catch(error =>
            create_alert(`Error retrieving layer ${layer_name}: ${error}`)
        );
}

function create_alert(message) {
    const alerts = document.getElementById("alerts");

    const div = document.createElement("div");
    div.classList.add("alert");

    const span = document.createElement("span");
    span.classList.add("closebtn");
    span.addEventListener("click", function(e) {
        this.parentElement.remove();
    }, false);
    span.innerHTML = "&times;";

    div.appendChild(span);
    div.appendChild(document.createTextNode(message));

    alerts.appendChild(div);
}

function delete_layer(t, event) {
    event.preventDefault();
    const layer = t.attributes.data.value;

    fetch(`/layer/${layer}`,  {
        method: "DELETE"
    })
        .then(resp => resp.json())
        .then(j => {
            if (j["status"] != "OK"){
                throw j["status"];
            }
            layers_control.removeLayer(wifi_layers[layer]);
            wifi_layers[layer].remove();
            delete wifi_layers[layer];
        })
        .catch(error => create_alert(`Error deleting layer: ${error}`));
}

function load_layers(enable_layers) {
    fetch("/layers/")
        .then((resp) => resp.json())
        .then((layers) => {

            layers.forEach(layer => {
                if(!wifi_layers.hasOwnProperty(layer)) {
                    wifi_layers[layer] = L.geoJSON(null, {
                        onEachFeature: onEachFeature
                    });

                    layers_control.addOverlay(
                        wifi_layers[layer],
                        `<span id="control-layer-${layer}">${layer}</span> <span class="layer-cross" data="${layer}" onclick="delete_layer(this, event);">&times</span>`
                    );
                }
            });

            if(enable_layers) {
                enable_layers.forEach(layer => {
                    document.getElementById(`control-layer-${layer}`)
                        .parentElement.parentElement.children[0]
                        .click();
                });
            }
        })
        .catch(e => create_alert(e));
}

function prepare_map() {
    var osm = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap'
    });
    var map = L.map('map', {
        center: [42.880616, -8.545627],
        zoom: 2,
        layers: [osm],
    });

    layers_control.addTo(map);

    return map;
}


window.onload = function () {
    main();
};
