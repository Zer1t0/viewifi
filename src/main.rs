use actix_files::NamedFile;
use actix_multipart::{form::tempfile::TempFile, form::MultipartForm};
use actix_web::{
    delete, get, http::StatusCode, post, web, App, HttpResponse, HttpServer,
    Responder, ResponseError,
};
use actix_web_static_files::ResourceFiles;
use rustls::{Certificate, PrivateKey, ServerConfig};
use rustls_pemfile::{certs, pkcs8_private_keys, rsa_private_keys};
use serde::Serialize;
use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::net::Ipv4Addr;
use std::path::{Path, PathBuf};
use std::process::ExitCode;

include!(concat!(env!("OUT_DIR"), "/generated.rs"));

const LAYERS_PATH: &str = "./layers/";
const LAYERS_EXT: &str = "kml";
const DEFAULT_BIND: &str = "127.0.0.1";

use clap::Parser;
use clap_verbosity_flag::{InfoLevel, Verbosity};

/// View your kml files
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Folder to store the wifi layers
    #[arg(short, long, default_value_t = LAYERS_PATH.to_string())]
    layers_dir: String,

    /// Port to listen
    #[arg(short, long, default_value_t = 5959)]
    port: u16,

    /// Address to listen
    #[arg(short, long, default_value_t = DEFAULT_BIND.parse().unwrap())]
    bind: Ipv4Addr,

    #[command(flatten)]
    verbose: Verbosity<InfoLevel>,

    /// Certificate for HTTPS
    #[arg(long, requires = "key")]
    cert: Option<String>,

    /// Private key for HTTPS certificate
    #[arg(long, requires = "cert")]
    key: Option<String>,
}

struct AppState {
    layers_dir: String,
}

#[actix_web::main]
async fn main() -> std::io::Result<ExitCode> {
    let args = Args::parse();

    env_logger::Builder::new()
        .filter_level(args.verbose.log_level_filter())
        .init();

    let tls_config = if let Some(cert_path) = args.cert {
        let key_path = args.key.unwrap();

        let cert_chain = load_cert_chain(&cert_path)?;
        let mut keys = load_keys(&key_path)?;

        if keys.is_empty() {
            log::error!("Private keys. (Encrypted keys are not supported)");
            return Ok(ExitCode::FAILURE);
        }

        let config_builder = ServerConfig::builder()
            .with_safe_defaults()
            .with_no_client_auth();

        let server_config =
            match config_builder.with_single_cert(cert_chain, keys.remove(0)) {
                Ok(server_config) => server_config,
                Err(_) => {
                    log::error!("Unable to load certificates");
                    return Ok(ExitCode::FAILURE);
                }
            };
        Some(server_config)
    } else {
        None
    };

    let layers_dir = args.layers_dir;
    fs::create_dir_all(&layers_dir)?;

    let proto = if tls_config.is_some() {
        "https"
    } else {
        "http"
    };

    log::info!("Running on {}://{}:{}", proto, args.bind, args.port);

    let mut http_server = HttpServer::new(move || {
        let static_generated = generate();
        App::new()
            .app_data(web::Data::new(AppState {
                layers_dir: layers_dir.to_owned(),
            }))
            .service(create_layer)
            .service(get_layer)
            .service(delete_layer)
            .service(web::redirect("/", "/static/index.html"))
            .service(ResourceFiles::new("/static", static_generated))
            .route("/layers", web::get().to(list_layers))
            .route("/layers/", web::get().to(list_layers))
    });

    http_server = match tls_config {
        Some(tls_config) => {
            http_server.bind_rustls((args.bind, args.port), tls_config)?
        }
        None => http_server.bind((args.bind, args.port))?,
    };

    http_server.run().await?;

    return Ok(ExitCode::SUCCESS);
}

fn load_cert_chain(
    cert_path: &str,
) -> Result<Vec<Certificate>, std::io::Error> {
    let cert_file = &mut BufReader::new(File::open(cert_path)?);
    let cert_chain = certs(cert_file)?.into_iter().map(Certificate).collect();
    return Ok(cert_chain);
}

fn load_keys(key_path: &str) -> Result<Vec<PrivateKey>, std::io::Error> {
    let keys: Vec<PrivateKey> = load_pkcs8_keys(key_path)?;
    if !keys.is_empty() {
        return Ok(keys);
    }
    return load_rsa_keys(key_path);
}

fn load_pkcs8_keys(key_path: &str) -> Result<Vec<PrivateKey>, std::io::Error> {
    let mut key_file = BufReader::new(File::open(key_path)?);

    let keys: Vec<PrivateKey> = pkcs8_private_keys(&mut key_file)?
        .into_iter()
        .map(PrivateKey)
        .collect();
    return Ok(keys);
}

fn load_rsa_keys(key_path: &str) -> Result<Vec<PrivateKey>, std::io::Error> {
    let mut key_file = BufReader::new(File::open(key_path)?);

    let keys = rsa_private_keys(&mut key_file)?
        .into_iter()
        .map(PrivateKey)
        .collect();

    return Ok(keys);
}

#[post("/layer/{name}")]
async fn create_layer(
    name: web::Path<String>,
    form: MultipartForm<Upload>,
    state: web::Data<AppState>,
) -> Result<impl Responder, LayerError> {
    let temp_file_path = form.file.file.path();
    let file_path = get_layer_filepath(&state.layers_dir, &name)?;

    std::fs::rename(temp_file_path, file_path)?;
    Ok(web::Json(LayerResponse::new("OK".into())))
}

#[get("/layer/{name}")]
async fn get_layer(
    name: web::Path<String>,
    state: web::Data<AppState>,
) -> Result<impl Responder, LayerError> {
    let file_path = get_layer_filepath(&state.layers_dir, &name)?;
    return Ok(NamedFile::open_async(file_path)
        .await?
        .set_content_type(mime::TEXT_XML));
}

async fn list_layers(
    state: web::Data<AppState>,
) -> Result<impl Responder, LayerError> {
    let dir_entries = fs::read_dir(&state.layers_dir)?;
    let mut layers = Vec::new();

    for dir_entry in dir_entries {
        let dir_entry = match dir_entry {
            Ok(de) => de,
            Err(_) => continue,
        };

        let path = dir_entry.path();
        if !is_layer_file(&path) {
            continue;
        }

        if let Some(filename) = path.file_stem() {
            if let Ok(filename) = filename.to_os_string().into_string() {
                layers.push(filename);
            }
        }
    }

    return Ok(web::Json(layers));
}

fn is_layer_file(path: &Path) -> bool {
    if let Some(extension) = path.extension() {
        return extension == LAYERS_EXT;
    }

    return false;
}

#[delete("/layer/{name}")]
async fn delete_layer(
    name: web::Path<String>,
    state: web::Data<AppState>,
) -> Result<impl Responder, LayerError> {
    let file_path = get_layer_filepath(&state.layers_dir, &name)?;

    fs::remove_file(file_path)?;
    return Ok(web::Json(LayerResponse::new("OK".into())));
}

fn sanitize_filename(name: &str) -> Result<String, String> {
    if name
        .chars()
        .any(|c| !(c.is_alphanumeric() || c == '-' || c == '_'))
    {
        return Err(format!("Invalid layer name"));
    }
    return Ok(name.into());
}

fn get_layer_filepath(layers_dir: &str, name: &str) -> Result<PathBuf, String> {
    let mut file_path = PathBuf::from(layers_dir);
    file_path.push(&format!("{}.{}", sanitize_filename(&name)?, LAYERS_EXT));
    return Ok(file_path);
}

#[derive(MultipartForm)]
pub struct Upload {
    file: TempFile,
}

#[derive(Debug)]
struct LayerError {
    response: LayerResponse,
}

impl std::fmt::Display for LayerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl From<String> for LayerError {
    fn from(err: String) -> Self {
        return Self {
            response: LayerResponse::new(err),
        };
    }
}

impl From<std::io::Error> for LayerError {
    fn from(err: std::io::Error) -> Self {
        return Self {
            response: LayerResponse::new(format!("{}", err)),
        };
    }
}

impl ResponseError for LayerError {
    fn status_code(&self) -> StatusCode {
        return StatusCode::OK;
    }
    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code()).json(self.response.clone())
    }
}

#[derive(Serialize, Debug, Clone)]
struct LayerResponse {
    status: String,
}

impl LayerResponse {
    fn new(status: String) -> Self {
        return Self { status };
    }
}
