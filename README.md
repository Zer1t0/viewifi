# viewifi

View your wifi kml maps locally\*.

> \*Note: You need internet connection in order to download the maps from tile.openstreetmap.org.

## Installation

You can get the viewifi from [Releases](https://gitlab.com/Zer1t0/viewifi/-/releases) or install it with cargo:

```
cargo install --git https://gitlab.com/Zer1t0/viewifi
```

## Run

```
viewifi
```

## Usage

In the web interface (default in http://127.0.0.1:5959/) you can upload your KML files to see them.

Additionally, you can manually copy your KML files (`.kml`) to the layers directory (by default `./layers`) and they will be loaded into the application as layers.

It should work with any KML file, but it was developed with [Kismet](https://www.kismetwireless.net/) KML files format in mind.


## HTTPS

You can create a pair cert/key with the following openssl command:
```
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 -days 365 -noenc
```

And then pass the certificate and key generated to enable HTTPS:
```
viewifi --cert cert.pem --key key.pem
```

## Attribution

[Wifi icons created by Gregor Cresnar - Flaticon](https://www.flaticon.com/free-icons/wifi)
